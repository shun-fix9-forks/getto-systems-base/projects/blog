# Version : 0.11.0

publish: ci-dockle-trivy

# Version : 0.10.0

publish: run-dockle

# Version : 0.9.0

publish: vim-markdownlint

# Version : 0.8.0

publish: elm-language-server

# Version : 0.7.0

publish docker-run-pipe

# Version : 0.6.1

fix: k8s cert-manager

# Version : 0.6.0

publish: k8s cert-manager

# Version : 0.5.1

fix: ruby-watch

# Version : 0.5.0

publish: ruby-filewatcher

# Version : 0.4.2

fix: content

# Version : 0.4.1

fix: content

# Version : 0.4.0

publish: cors-custom-header

# Version : 0.3.1

fix: variables

# Version : 0.3.0

publish: VSCode

# Version : 0.2.0

update: package

# Version : 0.1.0

add: gitlab-ci

